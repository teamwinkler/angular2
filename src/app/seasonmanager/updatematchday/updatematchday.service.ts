import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';

import { USERROLE } from '../../user-role.enum';
import { BetofficeService } from '../../betoffice.service';
import { SessionService } from 'src/app/session/session.service';

@Injectable()
export class UpdateMatchdayService extends BetofficeService {

    constructor(http: HttpClient, sessionService: SessionService) {
        super(http, sessionService);
    }

    findSeasons(): Observable<Array<Rest.SeasonJson>> {
        return this.http.get<Array<Rest.SeasonJson>>(this.rootUrl + 'season/list', {headers: this.createHeader()});
    }

    findGroups(seasonId: number): Observable<Array<Rest.GroupTypeJson>> {
        return this.http.get<Array<Rest.GroupTypeJson>>(this.rootUrl + 'season/' + seasonId + '/group/all', {headers: this.createHeader()});
    }

    findCurrent(seasonId: number): Observable<Rest.RoundJson> {
        return this.http.get<Rest.RoundJson>(this.rootUrl + 'season/' + seasonId + '/current', {headers: this.createHeader()});
    }

    findRounds(seasonId: number, groupId: number): Observable<Rest.SeasonJson> {
        return this.http.get<Rest.SeasonJson>(
            this.rootUrl + 'season/' + seasonId + '/group/' + groupId + '/round/all', {headers: this.createHeader()});
    }

    findRound(roundId: number, groupId: number): Observable<Rest.RoundAndTableJson> {
        return this.http.get<Rest.RoundAndTableJson>(
            this.rootUrl + 'season/roundtable/' + roundId + '/group/' + groupId, {headers: this.createHeader()});
    }

    nextRound(roundId: number): Observable<Rest.RoundAndTableJson> {
        return this.http.get<Rest.RoundAndTableJson>(
            this.rootUrl + 'season/roundtable/' + roundId + '/next', {headers: this.createHeader()});
    }

    prevRound(roundId: number): Observable<Rest.RoundAndTableJson> {
        return this.http.get<Rest.RoundAndTableJson>(
            this.rootUrl + 'season/roundtable/' + roundId + '/prev', {headers: this.createHeader()});
    }

    updateByOpenligaDb(roundId: number, groupId: number): Observable<Rest.RoundAndTableJson> {
        return this.http.post<Rest.RoundAndTableJson>(
            this.adminUrl + 'season/round/' + roundId + '/group/' + groupId + '/ligadbupdate', null, {headers: this.createHeader()});
    }

    createOrUpdateByOpenligaDb(roundId: number, groupId: number): Observable<Rest.RoundAndTableJson> {
        return this.http.post<Rest.RoundAndTableJson>(
            this.adminUrl + 'season/round/' + roundId + '/group/' + groupId + '/ligadbcreate', null, {headers: this.createHeader()});
    }

    updateMatchday(round: Rest.RoundJson, group: Rest.GroupTypeJson): Observable<Rest.RoundAndTableJson> {
        return this.http.post<Rest.RoundAndTableJson>(
            this.adminUrl + 'season/round/' + round.id + '/group/' + group.id + '/update', round, {headers: this.createHeader()});
    }
}
