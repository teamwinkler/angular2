export const environment = {
    // Der alte Filter mit Zeitzone: 'dd.MM.yyyy HH:mm': '+0200'
    dateTimeFormat: 'dd.MM.yyyy HH:mm',
    // TODO Wird mit der Community Edition abgeschafft...
    currentSeasonId: 33,

    production:         true,
    website:            'tippdiekistebier.de',
    rootUrl:            'https://tippdiekistebier.de/betoffice-jweb/bo/office/',
    authenticationUrl:  'https://tippdiekistebier.de/betoffice-jweb/bo/authentication/',
    adminUrl:           'https://tippdiekistebier.de/betoffice-jweb/bo/chiefoperator/',
    communityAdminUrl:  'http://tippdiekistebier.de/betoffice-jweb/bo/community-admin/',
    registerserviceUrl: 'https://cookie.gluehloch.de/registrationservice/registration/register',
    cookieserviceUrl:   'https://cookie.gluehloch.de/registrationservice/cookie/confirmCookie',
};
